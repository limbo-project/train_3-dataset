# REQUIRED parameters

# The ID of the Limbo dataset
LIMBO_DATASET_ID=train_3-dataset
# The ID of the Limbo dataset
LIMBO_DATASET_NAME="Bahnhoefe DB"
# The Limbo namespace
#LIMBO_NS=https://data.limbo-project.org/
# The graph of the Limbo dataset
#LIMBO_DATASET_GRAPH=$LIMBO_NS$LIMBO_DATASET_ID/
# The source namespace from the lifting portal
#SOURCE_NS=https://portal.limbo-project.org/
# The target namespace
#TARGET_NS=https://data.limbo-project.org/
# The Limbo dataset license
LIMBO_DATASET_LICENSE=NullLicense
# The mcloud namespace
#MCLOUD_NS=https://metadata.limbo-project.org/
# The dataset in the mcloud catalog that the Limbo dataset refers to
#MCLOUD_DATASET=dataset-Stationsdaten-RNI--552695060
# The distribution in the mcloud catalog that the Limbo dataset is based on
MCLOUD_DISTRIBUTION=DBSuS_Uebersicht_Bahnhoefe_Stand2018_03_1_csv
# The dataset this dataset links to (will be a list in later versions)
#LINK_DATASETS=(wikidata)

# OPTIONAL parameter(s)
# The version of the Limbo dataset
VERSION=0.0.3-SNAPSHOT

